/**
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene1 extends Phaser.Scene {
    constructor() {
        super();
        this.player = null;
        this.platforms = null;
        this.cursors = null;
        this.lastBomb = -1;
    }
    preload() {
        this.load.image('tiles', 'assets/tileset.png');
        this.load.tilemapTiledJSON('map', 'assets/level1.json');
        this.load.spritesheet('bubblelon',
            'assets/bubblelon.png',
            { frameWidth: 16, frameHeight: 16 }
        );
        this.load.spritesheet('bubble',
            'assets/bubble.png',
            { frameWidth: 16, frameHeight: 16 }
        );
        this.load.spritesheet('whale',
            'assets/whale.png',
            { frameWidth: 16, frameHeight: 16 }
        );
        this.load.spritesheet('zenchan',
            'assets/zenchan.png',
            { frameWidth: 16, frameHeight: 16 }
        );
        this.load.image('gate', 'assets/tile210.png');
    }
    create() {
        let self = this;
        this.cameras.main.setBounds(0, 0, 800, 1600);
        const map = this.make.tilemap({ key: 'map' });
        const tileset = map.addTilesetImage('level1', 'tiles');
        this.platforms = map.createStaticLayer('static', tileset, 0, 0);
        this._gates = map.createFromObjects('object', 211, { key: 'gate' });
        this.gates=new Array(this._gates.length);
        this._gates.forEach(gate => {
            self.physics.world.enable(gate);
            gate.body.setImmovable(true);
            gate.body.setAllowGravity(false);
            gate.gate_number=gate.data.list[0].value;
            this.gates[gate.gate_number]=gate;
        });
        map.setCollisionByExclusion([206]);
        this.map=map;
        this.anims.create({
            key: 'up',
            frames: this.anims.generateFrameNumbers('bubblelon', { start: 0, end: 0 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'down',
            frames: this.anims.generateFrameNumbers('bubblelon', { start: 0, end: 1 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'side',
            frames: this.anims.generateFrameNumbers('bubblelon', { start: 0, end: 4 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'shoot',
            frames: this.anims.generateFrameNumbers('bubblelon', { start: 5, end: 7 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'bubble',
            frames: this.anims.generateFrameNumbers('bubble', { start: 0, end: 6 }),
            frameRate: 5,
            repeat: -1
        });

        this.anims.create({
            key: 'whale',
            frames: this.anims.generateFrameNumbers('whale', { start: 0, end: 2 }),
            frameRate: 5,
            repeat: -1
        });

        this.anims.create({
            key: 'whale_bubble',
            frames: this.anims.generateFrameNumbers('whale', { start: 3, end: 5 }),
            frameRate: 5,
            repeat: -1
        });

        this.anims.create({
            key: 'zenchan',
            frames: this.anims.generateFrameNumbers('zenchan', { start: 0, end: 3 }),
            frameRate: 5,
            repeat: -1
        });

        this.anims.create({
            key: 'zenchan_bubble',
            frames: this.anims.generateFrameNumbers('zenchan', { start: 4, end: 6 }),
            frameRate: 5,
            repeat: -1
        });
        
        this.player = new Player(this, 50, 1580);
        this.cameras.main.startFollow(this.player);
        this.cursors = this.input.keyboard.createCursorKeys();
        this.physics.add.collider(this.platforms, this.player);

        this.physics.add.overlap(this.player, this.gates, function(p, g){
            if(g.gate_number==0){
                p.x=self.gates[1].x+50;
                p.y=self.gates[1].y;
            }else{
                p.x=self.gates[0].x+50;
                p.y=self.gates[0].y;
            }
        });
        
        this.whales=[];
        for(let i=0; i<10; i++){
            let whale=new Whale(this, 10+Math.random()*750, Math.random()*1600);
            this.whales.push(whale);
        }

        this.zenchans=[];
        for(let i=0; i<10; i++){
            let zenchan=new ZenChan(this, 10+Math.random()*750, Math.random()*1600);
            this.zenchans.push(zenchan);
        }

        this.bubbles=[];
        this.physics.add.collider(this.platforms, this.whales);
        this.physics.add.overlap(this.bubbles, this.whales, function(b, w){
            b.destroy();
            w.captured=true;
        });

        this.physics.add.collider(this.platforms, this.zenchans);
        this.physics.add.overlap(this.bubbles, this.zenchans, function(b, w){
            b.destroy();
            w.captured=true;
        });

        this.physics.add.overlap(this.player, this.whales, function(b, w){
            if(w.captured){
                w.disableBody(true, true);
            }else{
                b.disableBody(true, true);
            }
        });

        this.physics.add.overlap(this.player, this.zenchans, function(b, w){
            if(w.captured){
                w.disableBody(true, true);
            }else{
                b.disableBody(true, true);
            }
        });
    }

    update() {
        let current = Date.now();
        this.player.setVelocityX(0);
        //this.player.setVelocityY(0);
        if (this.cursors.left.isDown) {
            this.player.flipX = false;
            this.player.setVelocityX(-60);
            this.player.anims.play('side', true);
        } else if (this.cursors.right.isDown) {
            this.player.flipX = true;
            this.player.setVelocityX(60);
            this.player.anims.play('side', true);
        }  
        if(this.cursors.up.isDown && this.player.body.blocked.down) {
            this.player.setVelocityY(-250);
            this.player.anims.play('up', true);
        }
        if(this.cursors.space.isDown){
            this.player.anims.play('shoot', true);
            let b=new Bubble(this, this.player.x, this.player.y);
            this.bubbles.push(b);
            b.setVelocityX((this.player.flipX)?50:-50);
            b.body.setAllowGravity(false);
            b.setVelocityY(-1);
        }

        for(let whale of this.whales){
            whale.update();
        }

        for(let zenchan of this.zenchans){
            zenchan.update();
        }
        // if (this.cursors.space.isDown) {
        //     if ((current - this.lastBomb) > 1000) {
        //         this.lastBomb = current;
        //         let bomb = new Bomb(this, this.player.x + 9, this.player.y + 9);
        //     }
        // }
        
    }
}