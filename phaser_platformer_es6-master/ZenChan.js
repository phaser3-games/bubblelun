class ZenChan extends Phaser.Physics.Arcade.Sprite{
    constructor(scene, x, y){
        super(scene, x, y, "zenchan");
        this.scene=scene;
        scene.physics.world.enable(this);
        scene.add.existing(this);
        this.direction=-1;
        let self=this;
        this.captured=false;
        setInterval(function(){
            self.direction=self.direction*(-1);
        }, 2000);
    }

    update(){
        if(!this.captured){
            if(this.direction>0){
                this.flipX=true;
            }else{
                this.flipX=false;
            }
            this.setVelocityX(this.direction*30);
            this.anims.play('zenchan', true);
        }else{
            this.setVelocityX(0);
            this.setVelocityY(-30);
            this.anims.play("zenchan_bubble", true);
        }
    }
}